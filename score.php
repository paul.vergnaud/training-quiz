<?php

echo '<!Doctype html>

<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>A Bientôt </title>
  <link rel="stylesheet" href="reset.css">
  <link rel="stylesheet" href="style.css">
  <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
</head>
</html>';

$fichier= fopen('fichier-qcm-eco.txt', 'r+');
$all_lines = file('fichier-qcm-eco.txt');

$score = 0;
$a=1;

foreach ($all_lines as $line) {

    $tab= explode('##', $line);
    array_shift($tab);

    if (isset($_POST["order-".$a]))
    {
      $new_order = "(".$_POST["order-".$a].")";

      if ( $new_order == $tab[0]){
          $score++;
      }

      elseif ( $new_order == $tab[1]){
          $score++;
      }

      elseif ( $new_order == $tab[2]){
          $score++;
      }
  
  }


    $a++;
}

echo "<body>";

   echo '<section class="end">';

        echo '<div class="end-container">';
    
          echo '<div class="end-content-title">';
            echo '<h1 class="end-title">'; echo '<img src="./images/equerre.PNG" alt="square"> Etes-vous prêts pour les partiels ?';  echo '<img src="./images/equerre.PNG" alt="square">'; echo'</h1>';
            
          echo '</div>';
          echo '<p class="end-introduction">'; echo 'Bravo ! Vous êtes arrivés au bout du test. <br> Votre score est de : </p>'; echo ('<div class="end-introduction">'.$score."/30</div>"); 
          echo '<a href="accueil.html">'; echo '<button class="end-button"> Retour à l\'accueil'; echo '</button>';
    
    
    
        echo '</div>';
    
    echo '</section>';
  
echo "</body>";

?>

<!-- <body>

    <section class="end">

        <div class="end-container">
    
          <div class="end-content-title">
            <h1 class="end-title"> <img src="./images/equerre.PNG" alt="square"> Etes-vous prêts pour les partiels ?  <img src="./images/equerre.PNG" alt="square"> </h1>
            
          </div>
          <p class="end-introduction"> Bravo ! Vous êtes arrivés au bout du test. <br> Votre score est de : </p>
          <button class="end-button"> Retour à l'accueil </button>
    
    
    
        </div>
    
      </section>
  
</body>

</html>
-->