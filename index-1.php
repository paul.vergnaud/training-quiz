<?php
$fichier= fopen('fichier-qcm-eco.txt', 'r+');
$all_lines = file('fichier-qcm-eco.txt');


?>



<!Doctype html>

<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="reset.css">
  <link rel="stylesheet" href="style.css">
  <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Abril+Fatface" rel="stylesheet">
  <title> Plateforme de qcm </title>
</head>

<body>

  <section class="qcm">

    <header class="qcm-header">
      <img class="qcm-picture" src="./images/logo.png" alt="logo">
      <h1 class="qcm-title"> Training Quiz </h1>
    </header>

    <div class="qcm-introduction">
      <p class="qcm-intro-message">Répondez à cette série de question :</p>
    </div>


    <div class="qcm-button-question">
      <form method="post" action="traitement.php"></form>
        <select class="button__items-dropdown__select items-dropdown__select--active" id="menu" name="links" size="1"
          onchange="window.location.href=this.value;">
          <a href="#one">
            <option class="qcm__buttonItem" value=""> Questions </option>
          </a>
          <a href="#two">
            <option class="qcm__buttonItem" value=""> 1 </option>
          </a>
          <a href="#three">
            <option class="qcm__buttonItem" value=""> 2 </option>
          </a>
          <a href="#two">
            <option class="qcm__buttonItem" value=""> 3 </option>
          </a>
          <a href="#two">
            <option class="qcm__buttonItem" value=""> 4 </option>
          </a>
          <a href="#two">
            <option class="qcm__buttonItem" value=""> 5 </option>
          </a>
          <a href="#two">
            <option class="qcm__buttonItem" value=""> 6 </option>
          </a>
          <a href="#two">
            <option class="qcm__buttonItem" value=""> 7 </option>
          </a>
          <a href="#two">
            <option class="qcm__buttonItem" value=""> 8 </option>
          </a>
          <a href="#nine">
            <option class="qcm__buttonItem" value=""> 9 </option>
          </a>
          <a href="#ten">
            <option class="qcm__buttonItem" value=""> 10 </option>
          </a>
          <a href="#eleven">
            <option class="qcm__buttonItem" value=""> 11 </option>
          </a>
          <a href="#twelve">
            <option class="qcm__buttonItem" value=""> 12 </option>
          </a>
          <a href="#thirteen">
            <option class="qcm__buttonItem" value=""> 13 </option>
          </a>
          <a href="#fourteen">
            <option class="qcm__buttonItem" value=""> 14 </option>
          </a>
          <a href="index.html#fifteen">
            <option class="qcm__buttonItem" value=""> 15 </option>
          </a>
        </select>
      </form>
    </div>




    <!-- <a href="#one"> <button class="button-question"> Questions </button> </a> -->

    <div class="qcm-block">



      <form method="POST" action="score.php">

        <?php

$a=1;

foreach ($all_lines as $line) {

  $tab= explode('##', $line); 
  ?>

        <div class="qcm-question-block">
          <?php 
   
   echo ('<h3 class="question-displayer">'. $tab[0] .'</h3>'); // on affiche les questions 
   array_shift($tab); // séparer la question des réponses 
   ?>

          <?php

 foreach ($tab as $line) {
   
  $line = str_replace(")","", $line);
  $line = str_replace("(","", $line);
   ?>

         <br>  <br> <input type="checkbox" value="<?php echo $line; ?>" name="order-<?php echo $a; ?>">

          <!--bouton radio pour question -->
          <label class="view">
          <?php


 ?>
          <?php echo($line); ?></label><br>
          
          

          <?php
}
?>

        </div>

        <?php

  $a++;
}

 ?>

 

      <input class="index-button" type="submit" value="Valider"/>

      </form>




  </section>

</body>

</html>